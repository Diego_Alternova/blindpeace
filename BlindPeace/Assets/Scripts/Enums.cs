﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    Basic,
    Up,
    Down,
    YUp,
    YDown,
    Lake,
    End
}
